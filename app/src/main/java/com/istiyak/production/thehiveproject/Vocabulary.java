package com.istiyak.production.thehiveproject;

/**
 * Created by Istiyak on 05-Aug-16.
 */
public class Vocabulary {

    //=========== FIELDS ===========

    private int id;
    private String word;
    private String meaning;
    private String synonym;
    private String antonym;
    private String remarks;
    private int starMarked;

    //=========== CONSTRUCTOR ===========

    public Vocabulary() {

    }

    public Vocabulary(int id, String word, String meaning) {

        this.id = id;
        this.word = word;
        this.meaning = meaning;
    }

    //========= GETTER-SETTER =========

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getSynonym() {
        return synonym;
    }

    public void setSynonym(String synonym) {
        this.synonym = synonym;
    }

    public String getAntonym() {
        return antonym;
    }

    public void setAntonym(String antonym) {
        this.antonym = antonym;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getStarMarked() {
        return starMarked;
    }

    public void setStarMarked(int starMarked) {
        this.starMarked = starMarked;
    }

    //=========== PRIVATE METHODS ===========
}
