package com.istiyak.production.thehiveproject;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Istiyak on 05-Aug-16.
 */
public class CustomAdapter extends BaseAdapter {

    public Activity activity;
    private LayoutInflater inflater;
    private List<Vocabulary> vocabularyList;

    DbHelper dbHelper;

    public CustomAdapter(Activity activity, List<Vocabulary> vocabularyList) {
        this.activity = activity;
        this.vocabularyList = vocabularyList;
        dbHelper = new DbHelper(activity.getApplicationContext());
    }

    @Override
    public int getCount() {
        return vocabularyList.size();
    }

    @Override
    public Object getItem(int position) {
        return vocabularyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, null);
        }

        TextView word = (TextView) convertView.findViewById(R.id.word);
        TextView meaning = (TextView) convertView.findViewById(R.id.meaning);
        final Button starMark = (Button) convertView.findViewById(R.id.star_mark);

        final Vocabulary vocabulary = vocabularyList.get(position);
        word.setText(vocabulary.getId() + ". " + vocabulary.getWord());
        meaning.setText(vocabulary.getMeaning());

        if (vocabulary.getStarMarked() == 0) {
            starMark.setBackground(ContextCompat.getDrawable(this.activity.getApplicationContext(), R.drawable.non_star_marked));
        } else {
            starMark.setBackground(ContextCompat.getDrawable(this.activity.getApplicationContext(), R.drawable.star_marked));
        }

        starMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int starMarkValue = (vocabulary.getStarMarked() == 0) ? 1 : 0;
                dbHelper.update(vocabulary.getId(), starMarkValue);
                if (starMarkValue == 0) {
                    starMark.setBackground(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.non_star_marked));
                } else {
                    starMark.setBackground(ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.star_marked));
                }
            }
        });

        // genre
        return convertView;
    }
}
