package com.istiyak.production.thehiveproject;

import android.app.Application;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

/**
 * Created by Istiyak on 05-Aug-16.
 */
public class App extends Application {

    @Override
    public void onCreate() {

        super.onCreate();
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(1));
    }
}