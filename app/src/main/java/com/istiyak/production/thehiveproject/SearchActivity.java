package com.istiyak.production.thehiveproject;


import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

/**
 * Created by Istiyak on 11-Aug-16.
 */
public class SearchActivity extends AppCompatActivity{

    private DbHelper dbHelper;

    private ListView listview;

    private int noOfBtns;
    private Button[] buttons;

    public int ITEMS_PER_PAGE = 20;
    public int TOTAL_LIST_ITEMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        dbHelper = new DbHelper(this.getApplicationContext());
        listview = (ListView) findViewById(R.id.search_list);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            loadEverything(query, 0);
        }
    }

    private void loadEverything (String query, int index) {

        btnFooter(query);
        checkBtnBackGround(index);
        loadList(query, index);
    }

    private void btnFooter(String queryAsParam) {

        final String query = queryAsParam;
        TOTAL_LIST_ITEMS = dbHelper.getSearchResultVocabCount(query);

        int val = TOTAL_LIST_ITEMS % ITEMS_PER_PAGE;
        val = (val == 0) ? 0 : 1;
        noOfBtns = TOTAL_LIST_ITEMS / ITEMS_PER_PAGE + val;

        LinearLayout ll = (LinearLayout) findViewById(R.id.search_button_panel);
        buttons = new Button[noOfBtns];

//        ll.removeAllViews();

        for (int i = 0; i < noOfBtns; i++) {

            buttons[i] = new Button(this);
            buttons[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            buttons[i].setText("" + (i + 1));
            buttons[i].setTag(query);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT);
            ll.addView(buttons[i], lp);

            final int j = i;
            buttons[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadList((String) v.getTag(), j);
                    checkBtnBackGround(j);
                }
            });
        }
    }

    private void checkBtnBackGround(int index) {

        for (int i = 0; i < noOfBtns; i++) {
            if (i == index) {
                buttons[index].setBackgroundDrawable(getResources().getDrawable(R.drawable.button_background));
                buttons[i].setTextColor(getResources().getColor(android.R.color.black));
            } else {
                buttons[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                buttons[i].setTextColor(getResources().getColor(android.R.color.white));
            }
        }
    }

    private void loadList(String query, int number) {

        List<Vocabulary> vocabularyList = dbHelper.getSearchResultVocabList(query, number);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider_simple);
        CustomAdapter adapter = new CustomAdapter(this, vocabularyList);
        listview.setAdapter(adapter);
        listview.setDivider(dividerDrawable);
        adapter.notifyDataSetChanged();
    }
}