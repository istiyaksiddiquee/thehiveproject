package com.istiyak.production.thehiveproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Istiyak on 05-Aug-16.
 */
public class DbHelper extends SQLiteAssetHelper {

    //==================== FIELDS ==================

    private Context context;

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "vocabulary_db.db";

    private static final String TABLE_NAME = "VOCABULARY";
    private static final String ID_COLUMN = "ID";
    private static final String WORD_COLUMN = "WORD";
    private static final String MEANING_COLUMN = "MEANING";
    private static final String SYNONYM_COLUMN = "SYNONYM";
    private static final String ANTONYM_COLUMN = "ANTONYM";
    private static final String REMARKS_COLUMN = "REMARKS";
    private static final String STAR_MARKED_COLUMN = "STAR_MARKED";

    private static final String ORDER_BY = " ORDER BY ";
    private static final int RECORDS_PER_PAGE = 20;

    //==================== CONSTRUCTORS ==================

    public DbHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        setForcedUpgrade();
    }

    //==================== PUBLIC METHODS ==================

    public void update(int id, int starMarkValue) {

        SQLiteDatabase db = getReadableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(DbHelper.STAR_MARKED_COLUMN, starMarkValue);
        db.update(DbHelper.TABLE_NAME, contentValues, DbHelper.ID_COLUMN + " = " + id, null);
        db.close();

        return;
    }

    public List<Vocabulary> getSearchResultVocabList(String query, int pageNumber) {

        int startIndex = pageNumber * DbHelper.RECORDS_PER_PAGE;
        List<Vocabulary> vocabularyList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME + " WHERE " +
                DbHelper.WORD_COLUMN + " LIKE '%" + query + "%'" + DbHelper.ORDER_BY +
                DbHelper.WORD_COLUMN + " LIMIT " + DbHelper.RECORDS_PER_PAGE + " OFFSET " +
                startIndex, null);

        if (cursor != null) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Vocabulary vocabulary = cursorToVocabulary(cursor);
                vocabularyList.add(vocabulary);
                cursor.moveToNext();
            }
            cursor.close();
        }

        db.close();

        return vocabularyList;
    }

    public int getSearchResultVocabCount(String query) {

        int totalVocab = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + DbHelper.TABLE_NAME + " WHERE " +
                DbHelper.WORD_COLUMN + " LIKE '%" + query + "%'", null);

        if (cursor != null) {
            cursor.moveToFirst();
            totalVocab = cursor.getInt(0);
            cursor.close();
        }

        db.close();
        return totalVocab;
    }

    public List<Vocabulary> listAll(String alphabet, int pageNumber) {

        if (alphabet.equalsIgnoreCase("Star Marked")) {
            return getStarMarkedVocabularyList(pageNumber);
        }

        int startIndex = pageNumber * DbHelper.RECORDS_PER_PAGE;
        List<Vocabulary> vocabularyList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME + " WHERE " +
                DbHelper.WORD_COLUMN + " LIKE '" + alphabet + "%'" + DbHelper.ORDER_BY +
                DbHelper.WORD_COLUMN + " LIMIT " + DbHelper.RECORDS_PER_PAGE + " OFFSET " +
                startIndex, null);

        if (cursor != null) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Vocabulary vocabulary = cursorToVocabulary(cursor);
                vocabularyList.add(vocabulary);
                cursor.moveToNext();
            }
            cursor.close();
        }

        db.close();

        return vocabularyList;
    }

    public int getVocabularyCount(String alphabet) {

        if (alphabet.equalsIgnoreCase("Star Marked")) {
            return getStarMarkedVocabularyCount();
        }

        int totalVocab = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + DbHelper.TABLE_NAME + " WHERE " + DbHelper.WORD_COLUMN + " LIKE '" + alphabet + "%'", null);

        if (cursor != null) {
            cursor.moveToFirst();
            totalVocab = cursor.getInt(0);
            cursor.close();
        }

        db.close();
        return totalVocab;
    }

    //==================== PRIVATE METHODS ==================

    private List<Vocabulary> getStarMarkedVocabularyList(int pageNumber) {

        int startIndex = pageNumber * DbHelper.RECORDS_PER_PAGE;
        List<Vocabulary> vocabularyList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME + " WHERE " + DbHelper.STAR_MARKED_COLUMN
                + " = 1 " + DbHelper.ORDER_BY + DbHelper.WORD_COLUMN + " LIMIT " + DbHelper.RECORDS_PER_PAGE + " OFFSET " +
                startIndex, null);

        if (cursor != null) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Vocabulary vocabulary = cursorToVocabulary(cursor);
                vocabularyList.add(vocabulary);
                cursor.moveToNext();
            }
            cursor.close();
        }

        db.close();

        return vocabularyList;
    }

    private int getStarMarkedVocabularyCount() {

        int totalVocab = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + DbHelper.TABLE_NAME + " WHERE " + DbHelper.STAR_MARKED_COLUMN + " = 1", null);

        if (cursor != null) {
            cursor.moveToFirst();
            totalVocab = cursor.getInt(0);
            cursor.close();
        }

        db.close();
        return totalVocab;
    }

    private Vocabulary cursorToVocabulary(Cursor cursor) {

        Vocabulary vocabulary = new Vocabulary();

        vocabulary.setId(cursor.getInt(0));
        vocabulary.setWord(cursor.getString(1));
        vocabulary.setMeaning(cursor.getString(2));
        vocabulary.setSynonym(cursor.getString(3));
        vocabulary.setAntonym(cursor.getString(4));
        vocabulary.setRemarks(cursor.getString(5));
        vocabulary.setStarMarked(cursor.getInt(6));

        return vocabulary;
    }
}