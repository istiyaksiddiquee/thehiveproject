package com.istiyak.production.thehiveproject;


import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;


import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DbHelper dbHelper;

    private ListView listview;

    private int noOfBtns;
    private Button[] buttons;

    public int ITEMS_PER_PAGE = 20;
    public int TOTAL_LIST_ITEMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DbHelper(this.getApplicationContext());
        listview = (ListView) findViewById(R.id.list);

        loadEverything("A", 0);
    }

    private void loadEverything (String alphabet, int index) {

        btnFooter(alphabet);
        checkBtnBackGround(index);
        loadList(alphabet, index);
    }

    private void btnFooter(String alphabetAsParam) {

        final String alphabet = alphabetAsParam;
        TOTAL_LIST_ITEMS = dbHelper.getVocabularyCount(alphabet);

        int val = TOTAL_LIST_ITEMS % ITEMS_PER_PAGE;
        val = (val == 0) ? 0 : 1;
        noOfBtns = TOTAL_LIST_ITEMS / ITEMS_PER_PAGE + val;

        LinearLayout ll = (LinearLayout) findViewById(R.id.btnLay);
        buttons = new Button[noOfBtns];

        ll.removeAllViews();

        for (int i = 0; i < noOfBtns; i++) {

            buttons[i] = new Button(this);
            buttons[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            buttons[i].setText("" + (i + 1));
            buttons[i].setTag(alphabet);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            ll.addView(buttons[i], lp);

            final int j = i;
            buttons[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadList((String) v.getTag(), j);
                    checkBtnBackGround(j);
                }
            });
        }
    }

    private void checkBtnBackGround(int index) {

        for (int i = 0; i < noOfBtns; i++) {
            if (i == index) {
                buttons[index].setBackgroundDrawable(getResources().getDrawable(R.drawable.button_background));
                buttons[i].setTextColor(getResources().getColor(android.R.color.black));
            } else {
                buttons[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                buttons[i].setTextColor(getResources().getColor(android.R.color.white));
            }
        }
    }

    private void loadList(String alphabet, int number) {

        List<Vocabulary> vocabularyList = dbHelper.listAll(alphabet, number);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider_simple);
        CustomAdapter adapter = new CustomAdapter(this, vocabularyList);
        listview.setDivider(dividerDrawable);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        loadEverything(item.getTitle().toString(), 0);
        return super.onOptionsItemSelected(item);
    }
}